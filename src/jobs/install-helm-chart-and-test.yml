description: >
  Create helm chart an run upgrade --install on the helm chart
  and also test and the code

parameters:
  deployment-lane:
    description: The lane into which the deployment will occur
    type: string
    default: default
  tag:
    description: image tag
    type: string
  values-yaml:
    description: The values yaml file
    type: string
    default: values.yaml
  cluster-name:
    description: Cluster Name
    type: string
  service-name:
    description: Name of the service
    type: string
  aws-parameter-prefix:
    description: Name of the aws parameter prefix
    type: string
  helm-directory:
    description: Where the helm directory is located
    type: string
  parameter-store-json:
    description: Contains replacements for Parameters Store variables
    type: string
    default: >
      []
  deploy-postgres-in-cluster:
    description: >
      If true, then deploy postgres to the cluster. The username is "postgres", and the password by default will be "Welcome1",
      unless the parameter-store-json contains a reference to the Parameter Store variable that will set secrets.LOYALTY_DB_PASSWORD
    type: boolean
    default: false
  deploy-rabbitmq-in-cluster:
    description: >
      If true, then deploy RabbitMQ to the cluster. The username is "admin", and the password by default will be "Welcome1",
      unless the parameter-store-json contains a reference to the Parameter Store variable that will set secrets.RABBITMQ_PASSWORD
    type: boolean
    default: false
  purge-postgres-cluster-data:
    description: If true, then delete the persistent volumnes from the cluster for Postgresql
    type: boolean
    default: false
  purge-rabbitmq-cluster-data:
    description: If true, then delete the persistent volumnes from the cluster for RabbitMQ
    type: boolean
    default: false
  executor:
    type: executor
    default: aws-eks/python
executor: << parameters.executor >>
steps:
  - kubernetes/install
  - aws-eks/update-kubeconfig-with-authenticator:
      cluster-name: << parameters.cluster-name >>
  - helm/install-helm-client:
      version: v3.5.2
  - checkout:
      path: ./repo
  - run:
      name: Create namespace if it does not exist
      command: kubectl create namespace << parameters.deployment-lane >> || true
  - run:
      name: Remove Postgress and Rabbit from Cluster Namespace if present
      command: |
        helm repo add bitnami https://charts.bitnami.com/bitnami
        helm delete rabbitmq -n << parameters.deployment-lane >> || true
        helm delete postgres -n << parameters.deployment-lane >> || true
  - when:
      condition: << parameters.purge-postgres-cluster-data >>
      steps:
        - run:
            name: Purging Postgresql persistent volumes from cluster
            command: |
              kubectl delete pvc data-postgres-postgresql-0 -n << parameters.deployment-lane >> || true
  - when:
      condition: << parameters.purge-rabbitmq-cluster-data >>
      steps:
        - run:
            name: Purging RabbitMQ persistent volumes from cluster
            command: |
              kubectl delete pvc data-rabbitmq-0 -n << parameters.deployment-lane >> || true
  - when:
      condition: << parameters.deploy-postgres-in-cluster >>
      steps:
        - run:
            name: Deploy Postgres to Cluster
            command: |
              POSTGRES_PASSWORD=Welcome1
              for i in $(echo '<< parameters.parameter-store-json >>' | jq -c '.[]')
              do
                NAME=$(echo "${i}" | jq -r '.parameter')
                VALUE=$(echo "${i}" | jq -r '.valuesName')
                if [ "$VALUE" = "secrets.LOYALTY_DB_PASSWORD" ]; then
                  POSTGRES_PASSWORD=$(aws ssm get-parameters --with-decryption \
                  --names "/<< parameters.aws-parameter-prefix >>/<< parameters.deployment-lane >>/${NAME}" | jq -r '.Parameters[].Value')
                  echo "Set the password using Parameter Store value: /<< parameters.aws-parameter-prefix >>/<< parameters.deployment-lane >>/${NAME}"
                fi
              done
              helm repo add bitnami https://charts.bitnami.com/bitnami
              helm install postgres bitnami/postgresql --set postgresqlPassword=$POSTGRES_PASSWORD --set service.type=ClusterIP \
              -n << parameters.deployment-lane >> --timeout 5m0s --wait --wait-for-jobs
              echo " "
              echo "******************************************************************************************************"
              echo "Run this in your terminal to access PostgreSQL"
              echo "kubectl port-forward service/postgres-postgresql -n << parameters.deployment-lane >> 5432:5432"
              echo " "
              echo "Note: on the ports below (5432:5432), the first port is can be mapped to an open port on your laptop"
              echo "       The second port must always be 5432, as it maps to the service in the k8s cluster"
              echo "******************************************************************************************************"
              echo " "
  - when:
      condition: << parameters.deploy-rabbitmq-in-cluster >>
      steps:
        - run:
            name: Deploy RabbitMQ to Cluster
            command: |
              helm repo add bitnami https://charts.bitnami.com/bitnami
              RABBIT_ADMIN_PASSWORD=Welcome1
              for i in $(echo '<< parameters.parameter-store-json >>' | jq -c '.[]')
              do
                NAME=$(echo "${i}" | jq -r '.parameter')
                VALUE=$(echo "${i}" | jq -r '.valuesName')
                if [ "$VALUE" = "secrets.RABBITMQ_PASSWORD" ]; then
                  RABBIT_ADMIN_PASSWORD=$(aws ssm get-parameters --with-decryption \
                  --names "/<< parameters.aws-parameter-prefix >>/<< parameters.deployment-lane >>/${NAME}" | jq -r '.Parameters[].Value')
                  echo "Set the password using /<< parameters.aws-parameter-prefix >>/<< parameters.deployment-lane >>/${NAME}"
                fi
              done
              helm install rabbitmq bitnami/rabbitmq --set auth.username=admin,auth.password=$RABBIT_ADMIN_PASSWORD,service.type=ClusterIP \
              -n << parameters.deployment-lane >> --timeout 5m0s --wait --wait-for-jobs
              echo " "
              echo "*******************************************************************************************************"
              echo "Run this in your terminal to access RabbitMQ"
              echo "kubectl port-forward service/rabbitmq -n << parameters.deployment-lane >> 5671:5671"
              echo " "
              echo "Note: on the ports below (15672:15672), the first port is can be mapped to an open port on your laptop"
              echo "       The second port must always be 15672, as it maps to the service in the k8s cluster"
              echo "------------------------------------------------------------------------------------------------------"
              echo " "
  - run:
      name: Deploy application to EKS using Helm
      command: |
        #
        # Get password and connection string from parameter store
        #
        HELM_PARAMS=""
        for i in $(echo '<< parameters.parameter-store-json >>' | jq -c '.[]')
        do
          NAME=$(echo "${i}" | jq -r '.parameter')
          VALUE=$(echo "${i}" | jq -r '.valuesName')
          SECRET=$(aws ssm get-parameters --with-decryption \
          --names "/<< parameters.aws-parameter-prefix >>/<< parameters.deployment-lane >>/${NAME}" | jq -r '.Parameters[].Value')
          HELM_PARAMS+="--set ${VALUE}=${SECRET} "
        done
        #
        # Dry run the helm chart
        #
        helm upgrade --install << parameters.service-name >> ./repo/<< parameters.helm-directory >> \
        --values ./repo/<< parameters.helm-directory >>/<< parameters.values-yaml >> \
        --set commitHash=$(echo $CIRCLE_SHA1 | cut -c -7),\
        appsettingsValues.DeploymentLane=<< parameters.deployment-lane >>,\
        image.tag=<< parameters.tag >> \
        -n << parameters.deployment-lane >>  --create-namespace --dry-run \
        $HELM_PARAMS
        #
        # Install the helm chart - Wait for successfull deploy
        #
        helm upgrade --install << parameters.service-name >> ./repo/<< parameters.helm-directory >> \
        --values ./repo/<< parameters.helm-directory >>/<< parameters.values-yaml >> \
        --set commitHash=$(echo $CIRCLE_SHA1 | cut -c -7),\
        appsettingsValues.DeploymentLane=<< parameters.deployment-lane >>,\
        image.tag=<< parameters.tag >> \
        -n << parameters.deployment-lane >> --create-namespace --timeout 5m0s --wait --wait-for-jobs \
        $HELM_PARAMS
        helm ls -n << parameters.deployment-lane >>
        kubectl get pods -n << parameters.deployment-lane >>
  - run:
      name: Testing install
      command: |
        helm ls -o json -n << parameters.deployment-lane >>
        helm ls -n << parameters.deployment-lane >>
        kubectl get deployments -n << parameters.deployment-lane >>
        kubectl get pods -n << parameters.deployment-lane >>
        kubectl get pods -n << parameters.deployment-lane >> -o jsonpath='{.items[*].status.containerStatuses}' | jq
        echo " Check for this SHA: << parameters.tag >>"
